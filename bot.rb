require 'dotenv/load'
require 'discordrb'
require 'redis'

require_relative 'utilities.rb'
require_relative 'time.rb'
require_relative 'fun.rb'

token = ENV['DISCORD_TOKEN']
client_id = ENV['DISCORD_CLIENT_ID']

bot = Discordrb::Commands::CommandBot.new(
  token: token,
  client_id: client_id,
  ignore_bots: true,
  prefix: ';'
)

bot.include! UtilityCommands
bot.include! TimeCommands
bot.include! FunCommands

bot.run
