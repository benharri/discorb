module UtilityCommands
  extend Discordrb::Commands::CommandContainer

  command(
    :my_id,
    description: 'gets your discord user id'
  ) do |event|
    event.user.id
  end

  command(
    :roll,
    min_args: 0,
    max_args: 2,
    description: 'Generates a random number between 0 and 1, 1 and max or min and max.',
    usage: 'random [min/max] [max]'
  ) do |_event, min, max|
    if max
      rand(min.to_i..max.to_i)
    elsif min
      rand(1..min.to_i)
    else
      rand
    end
  end

  command(
    :eval,
    help_available: false
  ) do |event, *code|
    break unless event.user.id == ENV['MY_USER_ID'].to_i

    begin
      eval code.join(' ')
    rescue
      'an error occurred'
    end
  end

  command(
    :sys,
    help_available: false
  ) do |event, *args|
    break unless event.user.id == ENV['MY_USER_ID'].to_i

    begin
      event << '```'
      event << `#{args.join(' ')}`
      event << '```'
    rescue
      'an error occurred'
    end
  end

  command :embed do |event|
    event.channel.send_embed do |embed|
      embed.title = 'hi'
      embed.url = 'https://benharr.is'
      embed.description = "hi there #{event.user.mention}"
    end
  end

  command :about do |event|
    event.channel.send_embed do |embed|
      embed.title = "discorb"
      embed.url = 'https://gitlab.com/benharri/discorb'
      embed.description = 'discord bot made in ruby with discordrb by bn'
    end
  end



  command(
    :say,
    help_available: false
  ) do |event, *args|
    txt = event.content.downcase
    if txt.include? '@everyone'
      "can't mention everyone..."
    elsif txt.include? '@here'
      "can't mention here"
    else
      args.join(' ')
    end
  end



end
