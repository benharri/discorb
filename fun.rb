require 'httparty'

module FunCommands
  extend Discordrb::Commands::CommandContainer

  command(
    :magic8ball,
    description: 'ask the magic 8 ball a question',
    min_args: 1,
    usage: 'magic8ball [question]'
  ) do |event, *args|
    event.channel.send_embed do |embed|
      embed.title = ':8ball: Magic 8 Ball'
      embed.description = "Your question: *#{args.join(' ')}*\n**#{[
        'It is certain',
        'It is decidedly so',
        'Without a doubt',
        'Yes definitely',
        'You may rely on it',
        'As I see it, yes',
        'Most likely',
        'Outlook good',
        'Yes',
        'Signs point to yes',
        'Reply hazy try again',
        'Ask again later',
        'Better not tell you now',
        'Cannot predict now',
        'Concentrate and ask again',
        "Don't count on it",
        'My reply is no',
        'My sources say no',
        'Outlook not so good',
        'Very doubtful'
      ].sample}**"
      embed.color = 0xffffff
    end

  end

  command(
    :lenny,
    help_available: false
  ) do |event|
    "( ͡° ͜ʖ ͡°)"
  end

  command(
    :meh,
    help_available: false
  ) do |event|
    "¯\\\_(ツ)\_/¯"
  end

  command(
    :yesno,
    help_available: false
  ) do |event|
    event.channel.send_embed do |embed|
      response = HTTParty.get('https://yesno.wtf/api').parsed_response
      embed.title = response["answer"].upcase
      embed.image = Discordrb::Webhooks::EmbedImage.new(url: response["image"])
      embed.color = response["answer"] == 'yes' ? 0x006600 : 0x660000
    end
  end

  command(
    :hi,
    help_available: false
  ) do |event|
    [
      "hello #{event.user.mention}, how are you today?",
      'soup',
      'hi!',
      'henlo',
      'hallo!',
      'hola',
      'wassup',
    ].sample
  end

end
