module TimeCommands
  extend Discordrb::Commands::CommandContainer

  command(
    :time,
    description: 'tells the current time'
  ) do |event|
    Time.now.getlocal("-04:00").strftime("it's %I:%M%p")
  end

end
